"""This module is the package directory for catalogs to be built for kstars.
   Importing catalogs here makes them available."""

from .ngcic_steinicke import NGCICSteinicke
from .open_ngc import OpenNGC
from .abell_planetary_nebulae import AbellPlanetaryNebulae
from .sharpless_2 import Sharpless2
from .hickson_compact_groups import Hickson
from .lynds_dark import LyndsDark
