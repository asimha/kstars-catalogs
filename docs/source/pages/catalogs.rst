Available Catalogs
==================

The currently available catalogs are listed in the following auto-generated table.
See :class:`lib.catalogsdb.Catalog` for a description of the columns.

.. note:: You can download the latest build of the catalog by clicking on its id.

.. catalog-table::
